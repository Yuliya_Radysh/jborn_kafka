package com.example.jborn_consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JbornConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(JbornConsumerApplication.class, args);
    }

}
