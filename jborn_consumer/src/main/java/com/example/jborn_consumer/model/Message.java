package com.example.jborn_consumer.model;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "message")
@Builder
@Getter
@Setter
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id", updatable = false)
    private Long id;

    @Column(name="word", nullable = false)
    private String word;

    @Column(name="message_date",
            nullable = false, updatable = false)
    private LocalDateTime messageDate;

    public Message() {
    }

    public Message(Long id, String word, LocalDateTime messageDate) {
        this.id = id;
        this.word = word;
        this.messageDate = messageDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return Objects.equals(id, message.id) && Objects.equals(word, message.word) && Objects.equals(messageDate, message.messageDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, word, messageDate);
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", word='" + word + '\'' +
                ", messageDate=" + messageDate +
                '}';
    }
}