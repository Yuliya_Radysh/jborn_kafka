package com.example.jborn_consumer.controller;

import com.example.jborn_consumer.model.Message;
import com.example.jborn_consumer.service.MessageService;
import com.example.jborn_consumer.service.dto.MessageDto;
import com.example.jborn_consumer.service.messaging.event.MessageEvent;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class MessageController {
    private final MessageService messageService;
    private final ModelMapper modelMapper;

    @GetMapping("/consume")
    public MessageEvent consume(@RequestParam(name = "word") MessageEvent messageEvent) {
        Message message = messageService.save(modelMapper.map(messageEvent, MessageDto.class));
        return new MessageEvent(message.getWord());
    }
}
