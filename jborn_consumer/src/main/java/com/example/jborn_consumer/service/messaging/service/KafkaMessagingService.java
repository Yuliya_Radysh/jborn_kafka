package com.example.jborn_consumer.service.messaging.service;

import com.example.jborn_consumer.model.Message;
import com.example.jborn_consumer.service.MessageService;
import com.example.jborn_consumer.service.dto.MessageDto;
import com.example.jborn_consumer.service.messaging.event.MessageEvent;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@AllArgsConstructor
public class KafkaMessagingService {
    private static final String topicCreateMessage = "${topic.send-message}";
    private static final String kafkaConsumerGroupId = "${spring.kafka.consumer.group-id}";
    private final MessageService messageService;
    private final ModelMapper modelMapper;

    @Transactional
    @KafkaListener(topics = topicCreateMessage, groupId = kafkaConsumerGroupId, properties = {"spring.json.value.default.type=com.example.jborn_consumer.service.messaging.event.MessageEvent"})
    public MessageEvent createMessage(MessageEvent messageEvent) {
        log.info("Message consumed {}", messageEvent);
        messageService.save(modelMapper.map(messageEvent, MessageDto.class));
        return messageEvent;
    }
}
