package com.example.jborn_consumer.service;

import com.example.jborn_consumer.model.Message;
import com.example.jborn_consumer.service.dto.MessageDto;

public interface MessageService {
    Message save(MessageDto messageDto);
}