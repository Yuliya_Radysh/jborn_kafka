package com.example.jborn_consumer.service.impl;

import com.example.jborn_consumer.model.Message;
import com.example.jborn_consumer.repository.MessageRepository;
import com.example.jborn_consumer.service.MessageService;
import com.example.jborn_consumer.service.dto.MessageDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Slf4j
@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    @Override
    @Transactional
    public Message save(MessageDto messageDto) {
        Message message = Message.builder()
                .word(messageDto.getWord())
                .messageDate(LocalDateTime.now())
                .build();
        messageRepository.save(message);

        log.info("Save message: " + message.getWord());

        return message;
    }
}