package com.example.jborn_producer.service;

import com.example.jborn_producer.model.Message;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class Producer implements ApplicationRunner {

    private final MessagingService kafkaMessagingService;
    private final ModelMapper modelMapper;

    @Value("${delay}")
    private int delay;

    @Override
    public void run(ApplicationArguments args) {
        while (true) {
            Message message = MessageGenerator.generate();
            kafkaMessagingService.sendWord(modelMapper.map(message, MessageEvent.class));
            log.info("Send message from producer {}", message);
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                log.error("Cannot create massage!", e);
            }
        }
    }
}
