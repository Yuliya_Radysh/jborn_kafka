package com.example.jborn_producer.service;

import com.example.jborn_producer.client.ExternalMessageApi;
import feign.RetryableException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessagingService {

    private final ExternalMessageApi externalMessageApi;
    private final KafkaTemplate<String , MessageEvent> kafkaTemplate;

    @Value("${topic.send-message}")
    private String sendClientTopic;

    @Value("${target}")
    private String target;


    public void sendWord(MessageEvent messageEvent) {
        if (target.equals("kafka")) {

            kafkaTemplate.send(sendClientTopic, messageEvent.getWord(), messageEvent);

        } else if (target.equals("api")) {
            try {
                externalMessageApi.sendMessage(messageEvent.getWord());
            } catch (RetryableException e) {
                log.info("Consumer doesn't answer", e);
            }
        } else {
            log.error("Target not found");
        }
    }
}
