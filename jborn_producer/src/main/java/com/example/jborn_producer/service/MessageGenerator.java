package com.example.jborn_producer.service;

import com.example.jborn_producer.model.Message;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Random;

@Slf4j
@Component
public class MessageGenerator {
    public static Message generate() {
        Random random = new Random();
        int len = random.nextInt(100);
        char[] chars = new char[len];
        for (int i = 0; i < len; i++) {
            chars[i] = (char) ('a' + random.nextInt(26));
        }
        return new Message(new String(chars));
    }
}
