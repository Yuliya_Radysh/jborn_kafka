package com.example.jborn_producer.client;

import com.example.jborn_producer.service.MessageEvent;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "feignSender", url = "${consumerUrl}")
public interface ExternalMessageApi {

    @GetMapping
    MessageEvent sendMessage(@RequestParam(name = "word") String word);
}

